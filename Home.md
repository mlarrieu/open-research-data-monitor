---
layout: default
title: UGA open research data monitor
description: Université Grenoble Alpes open research data monitor
---


> Research is not only about writing papers, it's all also producing data and codes! This monitor aims to promote open research data, by gathering data produced by Université Grenoble Alpes. It's a prototype, published in February 2024, by the Cellule Data Grenoble Alpes. 

## Coverage
There is no simple solution to retrieve research data related to an institution. This monitor is a prototype: the results are interesting enough to be shared and there are limits in terms of coverage. Specifically, the monitor only includes data with a Digital Object Identifier. This allows us to promote persistent identifiers ([FAIR 1.1 principle](https://www.go-fair.org/fair-principles/f1-meta-data-assigned-globally-unique-persistent-identifiers/)) and to compare data efficiently. 



{% capture number %} {% include_relative assets/nb-dois.txt %} {% endcapture %} 
<h2>{{number}}</h2>

Is the total number of research data with a Digital Object Identifier (DOI) that we have found so far.


## "research data"?

We mean data that has been registered with [DataCite](https://datacite.org). Data included in the monitor are _findable_: they have a persistent identifier (DOI) attached to their metadata. This doesn't necessarily mean that they are _open_; a license may be missing or the access may be restricted.

## How to identify data? 

Mainly, we use APIs from data repositories to extract DOIs attached to Grenoble Alpes University. When possible, we search directly using Research Organization Registry (ROR) identifiers related to the university. Otherwise, we use literal expressions. Some research communities also create a DataCite account and register their data with their own tools. Therefore, our second way of identification is to track the DataCite accounts related to the university.


## Update

Graphs and their underlying data are updated each week on Monday automatically.

➔ [View the results!](/open-research-data-monitor/Monitor/)

