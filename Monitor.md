---
layout: default
title: UGA open research data monitor
description: Université Grenoble Alpes open research data monitor
---

<h2 class="monitor">How the quantity of research data evolved from 2016 to now?</h2>
<p class="monitor_img">
	<img class="monitor_img" src="https://gricad-gitlab.univ-grenoble-alpes.fr/mlarrieu/open-research-data-monitor-back/-/raw/main/2-produce-graph/hist-evol-datasets-per-repo.png?ref_type=heads"/>
</p>
This graph is a cumulative view on all the research data distributed by year of registration in DataCite and by repository. The number of data deposited each year is increasing overall. Zenodo and OSUG are the two main actors of findable research data at Grenoble Alpes University. The first one is the CERN repository, and the other one is a federation of research laboratories related to astronomy and earth sciences. Recherche Data Gouv is the French national research data repository.

<h2 class="monitor">What kind of research data?</h2> 
<p class="monitor_img">
	<img class="monitor_img" src="https://gricad-gitlab.univ-grenoble-alpes.fr/mlarrieu/open-research-data-monitor-back/-/raw/main/2-produce-graph/pie--datacite-type.png?ref_type=heads"/>
</p>
A typology is made by DataCite to describe each registered data. This graph shows the types we have included and their distribution. In the DataCite schema, the term `dataset` refers to a set of data with a defined structure (cf. [DataCite schema 4.5 documentation](https://datacite-metadata-schema.readthedocs.io/en/4.5/appendices/appendix-1/resourceTypeGeneral/)).

<h2 class="monitor">How many research data per year?</h2>
<p class="monitor_img">
	<img class="monitor_img" src="https://gricad-gitlab.univ-grenoble-alpes.fr/mlarrieu/open-research-data-monitor-back/-/raw/main/2-produce-graph/hist-quantity-year-type.png?ref_type=heads"/>
</p>
The graph shows the number of data deposited each year, and their DataCite type. It shows that more and more data is being shared every year.

<h2 class="monitor">Who are the DataCite clients?</h2>
<p class="monitor_img">
	<img class="monitor_img" src="https://gricad-gitlab.univ-grenoble-alpes.fr/mlarrieu/open-research-data-monitor-back/-/raw/main/2-produce-graph/pie--datacite-client.png?ref_type=heads"/>
</p>

This graph and the next one show the raw names of DataCite clients (_e.g._ `cern.zenodo` for Zenodo). A DataCite client is an entity that is able to register DOIs. The pie chart shows a distribution of research data by Datacite clients. It is a more precise distribution than the one shown in the first graph of this page. To describe the clients involved, we have produced a table 
[_all_datacite_clients_for_uga.csv_](https://gricad-gitlab.univ-grenoble-alpes.fr/mlarrieu/open-research-data-monitor-back/-/blob/main/1-enrich-with-datacite/all_datacite_clients_for_uga.csv?ref_type=heads), which is ordered by the number of registered DOIs. For quicker reading, let's note that some clients name correspond directly to a repository (Zenodo: `cern.zenodo`, Fighsare: `figshare.ars`, Dryad: `dryad.dryad`, Recherche Data Gouv: `rdg.prod`), and that client names starting with `inist` are part of the french research ecosystem.


<h2 class="monitor">What about the last month ?</h2> 
<p class="monitor_img">
	<img class="monitor_img" src="https://gricad-gitlab.univ-grenoble-alpes.fr/mlarrieu/open-research-data-monitor-back/-/raw/main/2-produce-graph/hist-last-datasets-by-client.png?ref_type=heads"/>
</p>
Finally, this graph shows the research data registered within the last 30 days, distributed by DataCite clients. It enables the [Cellule Data Grenoble Alpes](https://scienceouverte.univ-grenoble-alpes.fr/donnees/accompagner/cellule-data-grenoble-alpes/) to see all the latest activity.
