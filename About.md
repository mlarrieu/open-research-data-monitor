---
layout: default
title: UGA open research data monitor
description: Université Grenoble Alpes open research data monitor
---


## When? 

This project took off in November 2023 with the following questions: _how many open datasets are produced by the university?_ In February 2024, we launched this website to present our first results. It is a prototype: the results are already interesting, and several avenues could be explored to improve coverage.

## What technologies are used? 

- We collect data using various API repositories, including the one of DataCite
- We use the Research Organisation Registry and the French national repository HAL to get the identifiers of the entities of the university
- All our scripts are written in Python
- The website is build using Jekyll

## Where is our code?

Our code is shared under a free licence (GNU GPL Licence) and is available on the university forge, see the section below.

## How to cite? 

GRICAD, Cellule Data Grenoble Alpes, _UGA Open Research Data Monitor_ (2024), Grenoble Alpes University. [https://gricad-gitlab.univ-grenoble-alpes.fr/mlarrieu/open-research-data-monitor-back](https://gricad-gitlab.univ-grenoble-alpes.fr/mlarrieu/open-research-data-monitor-back)


## Who is involved? 

- Maxence Larrieu: collecting data, enrichment & visualisation
- Elias Chetouane: collecting data, program automation
- Louis Maritaud: website

As members of [GRICAD](https://gricad.univ-grenoble-alpes.fr/) & the [Cellule Data Grenoble Alpes](https://scienceouverte.univ-grenoble-alpes.fr/donnees/accompagner/cellule-data-grenoble-alpes/)



