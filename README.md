# Open Research Data Monitor

A monitor on open research data produced at Grenoble Alpes Univesrity : [mlarrieu.gricad-pages.univ-grenoble-alpes.fr/open-research-data-monitor](https://mlarrieu.gricad-pages.univ-grenoble-alpes.fr/open-research-data-monitor)



<br />
<br />

Explore the  500 last  DOIs in your browser : [datawrapper.dwcdn.net/5AGIk/4/](https://datawrapper.dwcdn.net/5AGIk/4/)